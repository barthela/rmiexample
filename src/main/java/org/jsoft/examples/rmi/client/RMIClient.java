package org.jsoft.examples.rmi.client;

import org.jsoft.examples.rmi.MySimpleClass;

import java.rmi.Naming;

/**
 * Created by andreas on 6/8/14.
 */
public class RMIClient {

    public static void main(String args[] ) {
        try {
            MySimpleClass c = (MySimpleClass) Naming.lookup("rmi://localhost/MySimpleClass");
            System.out.println("MySimpleClass: " + c.getSimpleString());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
