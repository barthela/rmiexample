package org.jsoft.examples.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by andreas on 6/8/14.
 */
public interface MySimpleClass extends Remote {

    public String getSimpleString() throws RemoteException;
    public void setSimpleString(String simpleString) throws RemoteException;

}
