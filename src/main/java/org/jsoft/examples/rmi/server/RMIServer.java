package org.jsoft.examples.rmi.server;

import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;

/**
 * Created by andreas on 6/8/14.
 */
public class RMIServer {

    public RMIServer() {

    }

    public static void main(String[] args) {
        try {
            LocateRegistry.createRegistry(1099);
            Naming.rebind("rmi://localhost/MySimpleClass", new MySimpleClassImpl());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
