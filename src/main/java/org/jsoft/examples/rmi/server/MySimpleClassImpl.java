package org.jsoft.examples.rmi.server;

import org.jsoft.examples.rmi.MySimpleClass;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created by andreas on 6/8/14.
 */
public class MySimpleClassImpl extends UnicastRemoteObject implements MySimpleClass {

    private String simpleString;

    public MySimpleClassImpl() throws RemoteException {
        simpleString = "Any String";
    }

    public String getSimpleString() throws RemoteException {
        return simpleString;
    }

    public void setSimpleString(String simpleString) throws RemoteException {
        this.simpleString = simpleString;
    }
}
